<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
</body>
</html>

<?php
    echo "<h3> Soal No 1 Greetings </h3>";
    greetings("Fitri");
    greetings("Ocha");
    //Code Function di sini
    function greetings($nama){
        echo "Halo ".$nama.", Selamat datang di my website! <br>";
    }
?>
<?php
    echo "<h3> Soal No 2 Greetings </h3>";
    reverseString("Roti");
    reverseString("Pemrograman Web");
    echo "<br>"; 

    //Code Function di sini
    function reverseString($kata1){
        $balik=reverse($kata1);
        echo $balik."<br>";
    }
    function reverse ($kata1){
        $panjangString = strlen($kata1);
        $tampung = "";

        for ($i = $panjangString-1;$i>=0;$i--){
            $tampung.=$kata1[$i];
        }
        return $tampung;
    }
?>
